using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ItInventoryApi.Models;
using AutoMapper;
using Microsoft.AspNetCore.JsonPatch;
using System.Collections.ObjectModel;

namespace ItInventoryApi.Controllers
{
    [Route("api/Sites/{siteId}/[controller]")]
    [ApiController]
    public class LocationsController : ControllerBase
    {
        private readonly ItInventoryContext _context;
        private readonly IMapper _mapper;

        private const String SITE_NOT_FOUND = "Site resource with id {0} was not found.";
        private const String DUPLICATE_ROOM_AT_SITE = "Room number {0} already exists at this site.";
        private const String MOVE_ASSETS_BEFORE_DELETING = "There are still assets listed at this location. A location must have no assets before it can be removed.";
        private static readonly ReadOnlyDictionary<string, List<string>> ValidPatchOperations = 
            new ReadOnlyDictionary<string, List<string>>
            (
                new Dictionary<string, List<string>>
                {
                    [nameof(Location.RoomNumber).ToLower()] = new List<string>(new string[] { "replace" }),
                    [nameof(Location.Description).ToLower()] = new List<string>(new string[] { "replace", "remove" })
                }
            );
        public LocationsController(ItInventoryContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Sites/1/Locations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Location>>> GetLocations(long siteId)
        {
            return await _context.Locations.Where(l => l.SiteId == siteId).ToListAsync();
        }

        // GET: api/Sites/1/Locations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<dynamic>> GetLocation(long siteId, long id)
        {
            var location = await _context.Locations.FirstOrDefaultAsync(l => l.Id == id && l.SiteId == siteId);

            if (location == null)
                return NotFound();

            var query =
                from type in _context.ItemTypes
                join model in _context.Models 
                    on type.Id equals model.ItemTypeId
                join brand in _context.Brands
                    on model.BrandId equals brand.Id
                join assetItem in _context.AssetItems
                    on model.Id equals assetItem.ModelId
                into assets
                from asset in assets.DefaultIfEmpty()
                where asset.LocationId == location.Id
                select new 
                { 
                    Type = type.Type,
                    MakeAndModel = brand.Name + " " + model.Number + (" " + model.AlternateName ?? "" ),
                    Asset = new 
                    {
                        asset.Id,
                        Number = asset.AssetNumber,
                        asset.SerialNumber
                    }
                };
            //EF Core no longer does client side grouping, have to do it yourself.
            var results = await query.ToListAsync();
            var groupedResults = results
                .GroupBy(result => result.Type)
                .Select(groupByType => new 
                    {
                        AssetType = groupByType.Key,
                        MakesAndModels = groupByType
                            .GroupBy(grp => grp.MakeAndModel)
                            .Select(groupByMakeModel => new 
                                {
                                    MakeAndModel = groupByMakeModel.Key,
                                    Assets = groupByMakeModel.Select(grp => grp.Asset)
                                }
                            )
                    }
                );
            return new 
            {
                location.RoomNumber,
                location.Description,
                AssetTypes = groupedResults.ToList(),
            };
        }

        // PATCH: api/Sites/1/Locations/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPatch("{id}")]
        public async Task<IActionResult> PatchLocation(long siteId, long id, [FromBody] JsonPatchDocument<Location> patchDoc)
        {
            //validate patch doc first to avoid database access in case invalid patch document?
            var location = await _context.Locations.FirstOrDefaultAsync(l => l.Id == id && l.SiteId == siteId);

            if (location == null)
                return NotFound();

            var patchDocErrors = patchDoc.Validate(nameof(Location), ValidPatchOperations);

            if (patchDocErrors != null)
                return BadRequest(patchDocErrors);

            patchDoc.ApplyTo(location);
            TryValidateModel(location);
            await ValidateEntityAsync(location, method: "PATCH");

            if(!ModelState.IsValid)
                return BadRequest(ModelState.Errors());

            _context.Entry(location).State = EntityState.Modified;
            await _context.SaveChangesAsync();
            return NoContent();
        }

        // POST: api/Locations
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost]
        public async Task<ActionResult<LocationViewModel>> PostLocation(long siteId, Location location)
        {
            //Needed so API users don't supply this themselves. This is the easiest solution to this.
            location.Id = 0;
            location.SiteId = siteId;
            await ValidateEntityAsync(location);

            if (!ModelState.IsValid)
                return BadRequest(ModelState.Errors());

            _context.Locations.Add(location);
            await _context.SaveChangesAsync();

            var viewModel = _mapper.Map<LocationViewModel>(location);
            return CreatedAtAction("PostLocation", new { id = location.Id }, viewModel);
        }

        // DELETE: api/Sites/1/Locations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Location>> DeleteLocation(long siteId, long id)
        {
            var location = await _context.Locations.FirstOrDefaultAsync(l => l.Id == id && l.SiteId == siteId);

            if (location == null)
            {
                return NotFound();
            }

            var locationAssetsCount = await _context.Entry(location)
                .Collection(a => a.AssetItems)
                .Query()
                .CountAsync();
            
            if (locationAssetsCount != 0)
            {
                return StatusCode(422, new { error = MOVE_ASSETS_BEFORE_DELETING });
            }

            _context.Locations.Remove(location);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool LocationExists(long id)
        {
            return _context.Locations.Any(e => e.Id == id);
        }

        private async Task<bool> RelatedSiteExistsAsync(long id)
        {
            return await _context.Sites.AnyAsync(s => s.Id == id);
        }

        private async Task<bool> LocationIsUniqueToSiteAsync(long id, ushort roomNumber, long siteId)
        {
            return !(await _context.Locations.AnyAsync(l => l.Id != id && l.RoomNumber == roomNumber && l.SiteId == siteId));
        }

        private async Task ValidateEntityAsync(Location location, string method = "POST")
        {
            var uniqueToSite = await LocationIsUniqueToSiteAsync(location.Id, location.RoomNumber, location.SiteId);
            if (!uniqueToSite)
                ModelState.AddModelError(nameof(Location.RoomNumber), string.Format(DUPLICATE_ROOM_AT_SITE, location.RoomNumber));

            if (method == "POST")
            {
                var siteExists = await RelatedSiteExistsAsync(location.SiteId);
                if (!siteExists)
                    ModelState.AddModelError(nameof(Location.SiteId), string.Format(SITE_NOT_FOUND, location.SiteId));
            }
        }
    }
}
