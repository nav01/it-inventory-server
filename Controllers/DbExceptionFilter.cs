using System;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Npgsql;

namespace ItInventoryApi.Controllers
{
    //Removes a lot of boilerplate code in controller methods.  Non database exceptions
    //should be caught and handled within the controller but this will return a 500 
    //response if a non db exception is caught.

    //This is a fallback in the case that validation (which should be implemented by you)
    //does not catch the constraint violations first.  The errors given to end user here 
    //are vague because of that.
    public class DbExceptionFilter : IExceptionFilter 
    {
        private const string UNIQUE_VIOLATION = "23505";
        private const string FOREIGN_KEY_VIOLATION = "23503";
        private const string NOT_NULL_VIOLATION = "23502";
        private struct Error
        {
            public string message { get; set;}
        }

        public async void OnException(ExceptionContext context)
        {
            if (context.Exception.InnerException is PostgresException postgresException)
            {   
                var httpResponse = context.HttpContext.Response;

                switch(postgresException.SqlState)
                {
                    case UNIQUE_VIOLATION:
                        var errUnique = new {error = "One or more submitted values already exist."};
                        await WriteJsonErrorResponse(httpResponse, HttpStatusCode.BadRequest, errUnique);
                        return;
                    case FOREIGN_KEY_VIOLATION:
                        var errFK = new {error = "One or more values that were expected to already exist could not be found."};
                        await WriteJsonErrorResponse(httpResponse, HttpStatusCode.NotFound, errFK);
                        return;
                    case NOT_NULL_VIOLATION:
                        var errNull = new {error = "A required field is missing."};
                        await WriteJsonErrorResponse(httpResponse, HttpStatusCode.BadRequest, errNull);
                        return;
                }
            }
            else if (context.Exception is DbUpdateException)
            {
                if (context.HttpContext.Request.Method == "DELETE")
                {
                    var response = context.HttpContext.Response;
                    response.StatusCode = (int) HttpStatusCode.NotFound;
                    await response.WriteAsync("");
                }
            }
            else if (context.Exception is InvalidOperationException && context.HttpContext.Request.Method == "PATCH")
            {
                var error = new {error = "Attempted to edit an uneditable field."};
                await WriteJsonErrorResponse(context.HttpContext.Response, HttpStatusCode.BadRequest, error);
                return;
            }
            else 
            {
                var error = new { error = "Unexpected Server Error"};
                await WriteJsonErrorResponse(context.HttpContext.Response, HttpStatusCode.InternalServerError, error);
                return;
            }
        }

        private async Task WriteJsonErrorResponse(HttpResponse response, HttpStatusCode statusCode, dynamic error)
        {
            response.ContentType = "application/json";
            response.StatusCode = (int) statusCode;
            await response.Body.WriteAsync(Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(error)));
        }
    }
}