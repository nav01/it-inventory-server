using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ItInventoryApi.Models;

using Microsoft.AspNetCore.JsonPatch;
using AutoMapper;
using Microsoft.EntityFrameworkCore.Query;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ItInventoryApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class AssetItemsController : ControllerBase
    {
        private readonly ItInventoryContext _context;
        private readonly IMapper _mapper;
        private const string LOCATION_NOT_FOUND = "Location resource with id {0} was not found.";
        private const string MODEL_NOT_FOUND = "Model resource with id {0} was not found.";
        private const string ASSET_ALREADY_EXISTS = "Asset {0} already exists.";
        private const string ASSET_AND_SERIAL_NOT_NULL = "You must supply at least one or both Asset Number and Serial Numbers.";
        private static readonly ReadOnlyDictionary<string, List<string>> ValidPatchOperations = 
            new ReadOnlyDictionary<string, List<string>>
            (
                new Dictionary<string, List<string>>
                {
                    [nameof(AssetItem.LocationId).ToLower()] = new List<string>(new string[] { "replace" }),
                }
            );

        public AssetItemsController(ItInventoryContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/AssetItems/5
        [HttpGet("[controller]/{assetOrSerial}")]
        public async Task<ActionResult<AssetItemViewModel>> GetAssetItem(string assetOrSerial)
        {
            var assetItem = await GetByAssetOrSerialAsync(assetOrSerial);
            
            if (assetItem == null)
                return NotFound();
            else 
                return _mapper.Map<AssetItemViewModel>(assetItem);
        }

        // PATCH: api/AssetItems/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPatch("[controller]/{id}")]
        public async Task<ActionResult<AssetItem>> PatchAssetItem(long id, [FromBody] JsonPatchDocument<AssetItem> patchDoc)
        {   
            var patchDocErrors = patchDoc.Validate(nameof(AssetItem), ValidPatchOperations);
            if (patchDocErrors != null)
                return BadRequest(patchDocErrors);

            var assetItem = await _context.AssetItems.FindAsync(id);

            if (assetItem == null)
                return NotFound();

            patchDoc.ApplyTo(assetItem, ModelState);
            TryValidateModel(assetItem);
            await ValidateEntityAsync(assetItem, method: "PATCH");

            if(!ModelState.IsValid)
                return BadRequest(ModelState);
                
            _context.Entry(assetItem).State = EntityState.Modified;
            await _context.SaveChangesAsync();            
            return NoContent();
        }

        // POST: api/AssetItems
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for
        // more details see https://aka.ms/RazorPagesCRUD.
        [HttpPost("[controller]")]
        public async Task<ActionResult<AssetItem>> PostAssetItem(AssetItem assetItem)
        {
            //Needed so API users don't supply this themselves. This is the easiest solution to this.
            assetItem.Id = 0;
            await ValidateEntityAsync(assetItem);

            if(!ModelState.IsValid)
                return BadRequest(new { errors = ModelState.Errors()});

            _context.AssetItems.Add(assetItem);
            await _context.SaveChangesAsync();
            
            //TODO: avoid refetching assetitem data, as it is already loaded
            var fullAsset = await GetById(assetItem.Id);

            if(!(fullAsset == null))
            {
                var assetToView = _mapper.Map<AssetItemViewModel>(fullAsset);
                return CreatedAtAction("PostAssetItem", new { id = assetItem.Id }, assetToView);
            }

            //Should be a rare occurrence, if ever.
            return NotFound("Asset was deleted by another user before it could be returned.");
        }

        // DELETE: api/AssetItems/5
        [HttpDelete("[controller]/{id}")]
        public async Task<ActionResult<AssetItem>> DeleteAssetItem(long id)
        {
            var assetItem = await _context.AssetItems.FindAsync(id);

            if (assetItem == null)
                return NotFound();

            _context.AssetItems.Remove(assetItem);
            await _context.SaveChangesAsync();

            return NoContent();
        }
        private async Task<bool> AssetItemExistsAsync(long id)
        {
            return await _context.AssetItems.AnyAsync(e => e.Id == id);
        }
        private async Task<bool> RelatedLocationExistsAsync(long locationId)
        {
            return await _context.Locations.AnyAsync(e => e.Id == locationId);
        }
        private async Task<bool> RelatedModelExistsAsync(long modelId)
        {
            return await _context.Locations.AnyAsync(e => e.Id == modelId);
        }
        private async Task<bool> AssetIsUnique(long id, string assetNumber)
        {
            return !(await _context.AssetItems.AnyAsync(a => a.Id != id && a.AssetNumber == assetNumber));
        }

        //TODO - Possibly consider optimizing data fetching or make it look nicer.
        private IIncludableQueryable<AssetItem, ItemType> LoadRelatedData()
        {
            return _context.AssetItems
                .Include(a => a.Location)
                .ThenInclude(l => l.Site)
                .Include(a => a.Model)
                .ThenInclude(m => m.Brand)
                .Include(a => a.Model)
                .ThenInclude(m => m.ItemType);
        }
        private Task<AssetItem> GetByAssetOrSerialAsync(string assetOrSerial) 
        {
            return LoadRelatedData()
                .Where(a => a.AssetNumber == assetOrSerial || a.SerialNumber == assetOrSerial)
                .FirstOrDefaultAsync();
        }

        private Task<AssetItem> GetById(long id)
        {
            return LoadRelatedData()
                .Where(a => a.Id == id)
                .FirstOrDefaultAsync();
        }

        private async Task ValidateEntityAsync(AssetItem assetItem, String method = "POST")
        {
            //TODO: does this belong here or in the model definition? Checking this using IValidatableObject
            //was interfering with consistent error reporting.
            if (string.IsNullOrWhiteSpace(assetItem.AssetNumber) && string.IsNullOrWhiteSpace(assetItem.SerialNumber))
            {
                ModelState.AddModelError(nameof(AssetItem.AssetNumber), ASSET_AND_SERIAL_NOT_NULL);
                ModelState.AddModelError(nameof(AssetItem.SerialNumber), ASSET_AND_SERIAL_NOT_NULL);
            }

            var locationExists = await RelatedLocationExistsAsync(assetItem.LocationId);
            if(!locationExists)
                ModelState.AddModelError(nameof(AssetItem.LocationId), string.Format(LOCATION_NOT_FOUND, assetItem.LocationId));

            var modelExists = await RelatedModelExistsAsync(assetItem.ModelId);
            if(!modelExists)
                ModelState.AddModelError(nameof(AssetItem.ModelId), string.Format(MODEL_NOT_FOUND, assetItem.ModelId));

            if (method == "POST")
            {
                var assetIsUnique = await AssetIsUnique(assetItem.Id, assetItem.AssetNumber);
                if(!assetIsUnique)
                    ModelState.AddModelError(nameof(AssetItem.AssetNumber), string.Format(ASSET_ALREADY_EXISTS, assetItem.AssetNumber));
            }
        }
    }
}
