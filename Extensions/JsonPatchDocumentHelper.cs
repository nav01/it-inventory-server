using Microsoft.AspNetCore.JsonPatch;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;

public static class JsonPatchDocumentHelper
{
    public static Dictionary<string, string[]> Validate<T>(
        this JsonPatchDocument<T> patchDoc,
        string errorKey,
        ReadOnlyDictionary<string, List<string>> validOperations
    ) where T : class
    {       
        var errorsDict = new Dictionary<string, string[]>();

        if (patchDoc.Operations.Count > validOperations.Count)
        {
            errorsDict[errorKey] = new string [] { $"Number of operations in patch document for this resource cannot exceed {validOperations.Count}." };
            return errorsDict;
        }

        var errors = new List<string>();

        patchDoc.Operations.ForEach(a => 
        {
            var trimmedPath = a.path.TrimStart('/').ToLower();
            if (validOperations.ContainsKey(trimmedPath))
            {
                if(!validOperations[trimmedPath].Contains(a.op, StringComparer.OrdinalIgnoreCase))
                    errors.Add($"'{a.op}' is not a valid operation for path '{a.path}' for this resource.");
            }
            else
                errors.Add($"'{a.path}' is not a valid patch path for this resource.");      
        });
        
        if (errors.Count > 0)
        {
            errorsDict[errorKey] = errors.ToArray();
            return errorsDict;
        } 
        else 
            return null;
    }
}