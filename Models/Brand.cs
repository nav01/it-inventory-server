using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItInventoryApi.Models {
    public class Brand {
        public long Id { get; set; }
        [Required, StringLength(30, ErrorMessage="Brand name cannot exceed 30 characters.")]
        public string Name { get; set; }
        public List<Model> Models { get; set; }
    }
}