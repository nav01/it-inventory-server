using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItInventoryApi.Models 
{
    public class AssetItem 
    {
        public long Id { get; set; }
        [StringLength(12, MinimumLength=8, ErrorMessage="Asset number should be at least 8 characters and no longer than 12.")]
        public string AssetNumber { get; set; }
        [StringLength(30, ErrorMessage="Serial number should be less than 30 characters.")]
        public string SerialNumber { get; set; }
        [Required]
        public long ModelId { get; set; }
        public Model Model { get; set; }
        [Required]
        public long LocationId { get; set; }
        public Location Location { get; set; }
    }

    public class AssetItemViewModel
    {
        public long Id { get; set; }
        public string AssetNumber { get; set; }
        public string SerialNumber { get; set; }
        public string ItemType { get; set; }
        public string ModelNumber { get; set; }
        public string ModelName { get; set; }
        public string Site { get; set; }
        public ushort RoomNumber { get; set; }
        public string RoomDescription { get; set; }
    }

    public class AssetItemLocationChild
    {
        public long Id { get; set; }
        public string AssetNumber { get; set; }
        public string SerialNumber { get; set; }
        public string ModelNumber { get; set; }
        public string ModelName { get; set; }
    }
}