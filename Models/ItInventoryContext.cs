using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace ItInventoryApi.Models
{
    public class ItInventoryContext : DbContext
    {
        private readonly IConfiguration _configuration;
        public ItInventoryContext(DbContextOptions<ItInventoryContext> options, IConfiguration configuration)
            : base(options)
        {
            _configuration = configuration;
        }

        public DbSet<Site> Sites { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<AssetItem> AssetItems { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<ItemType> ItemTypes { get; set; }
        public DbSet<Model> Models { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region UniqueConstraints
            modelBuilder.Entity<Site>().HasIndex(
                site => 
                    new { site.Name })
                    .IsUnique()
                    .HasName("Site name must be unique");

            modelBuilder.Entity<Brand>().HasIndex(
                brand => new { brand.Name }).IsUnique();

            modelBuilder.Entity<ItemType>().HasIndex(
                itemType => new { itemType.Type }).IsUnique();

            modelBuilder.Entity<Model>().HasIndex(
                model => new { model.BrandId, model.Number }).IsUnique();

            modelBuilder.Entity<AssetItem>().HasIndex(
                item => new { item.AssetNumber }).IsUnique();

            modelBuilder.Entity<AssetItem>().HasIndex(
                item => new { item.ModelId, item.SerialNumber }).IsUnique();

            modelBuilder.Entity<Location>().HasIndex(
                location => new { location.SiteId, location.RoomNumber }).IsUnique();
            #endregion

            #region SeedData
            modelBuilder.Entity<Site>().HasData(
                new { Id = 1L, Name = "Kerman High School" });

            modelBuilder.Entity<Location>().HasData(
                new { SiteId = 1L, Id = 1L, RoomNumber = ((ushort) 1003), Description = "IT Office - Library" });

            modelBuilder.Entity<ItemType>().HasData(
                new { Id = 1L, Type = "Chromebook" },
                new { Id = 2L, Type = "Laptop" },
                new { Id = 3L, Type = "Monitor" },
                new { Id = 4L, Type = "Desktop" });

            modelBuilder.Entity<Brand>().HasData(
                new { Id = 1L, Name = "Samsung" },
                new { Id = 2L, Name = "Dell" });

            modelBuilder.Entity<Model>().HasData(
                new Model{ BrandId = 1L, ItemTypeId = 1L,Id = 1L, Number = "XE521QAB", AlternateName = "Chromebook Plus V2" }
            );
            #endregion
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql(_configuration["DB:ConnectionString"]);
    }
}