using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItInventoryApi.Models {
    public class ItemType {
        public long Id { get; set; }
        [Required, StringLength(25, ErrorMessage="Item type cannot exceed 25 characters.")]
        public string Type { get; set; }

        public List<Model> Models { get; set; }
    }
}