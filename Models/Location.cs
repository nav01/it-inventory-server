using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItInventoryApi.Models 
{
    public class Location 
    {
        public long Id { get; set; }
        [Required]
        public ushort RoomNumber { get; set; }
        [StringLength(30, ErrorMessage = "Description must be no longer than 30 characters.")]
        public string Description { get; set; }
        [Required]
        public long SiteId { get; set; }
        public Site Site { get; set; }

        public List<AssetItem> AssetItems { get; set; }
    }

    public class LocationViewModel
    {
        public long Id { get; set; }
        public ushort RoomNumber { get; set; }
        public string Description { get; set; }
        public List<AssetItemLocationChild> AssetItems { get; set; }
    }

}