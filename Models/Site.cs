using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ItInventoryApi.Models {
    public class Site {
        public long Id { get; set; }
        [Required, StringLength(30, ErrorMessage="Site name cannot exceed 30 characters.")]
        public string Name { get; set; }
        public List<Location> Locations { get; set; }
    }
}