using System.ComponentModel.DataAnnotations;

namespace ItInventoryApi.Models {
    public class Model {
        public long Id { get; set; }
        [Required]
        public string Number { get; set; }
        public string AlternateName { get; set; }

        public long BrandId { get; set; }
        public Brand Brand { get; set; }

        public long ItemTypeId { get; set; }
        public ItemType ItemType { get; set; }
    }
}