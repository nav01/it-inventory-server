using System.Collections.Generic;
using AutoMapper;

namespace ItInventoryApi.Models 
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<AssetItem, AssetItemViewModel>()
                .ForMember(dest => dest.ItemType, act => act.MapFrom(src => src.Model.ItemType.Type))
                .ForMember(dest => dest.ModelNumber, act => act.MapFrom(src => src.Model.Number))
                .ForMember(dest => dest.ModelName, act => act.MapFrom(src => src.Model.AlternateName))
                .ForMember(dest => dest.Site, act => act.MapFrom(src => src.Location.Site.Name))
                .ForMember(dest => dest.RoomNumber, act => act.MapFrom(src => src.Location.RoomNumber))
                .ForMember(dest => dest.RoomDescription, act => act.MapFrom(src => src.Location.Description));
            
            CreateMap<AssetItem, AssetItemLocationChild>()
                .ForMember(dest => dest.ModelName, act => act.MapFrom(src => src.Model.AlternateName))
                .ForMember(dest => dest.ModelNumber, act => act.MapFrom(src => src.Model.Number));
            
            CreateMap<Location, LocationViewModel>()
                .ForMember(dest => dest.AssetItems, act => act.MapFrom(
                    src => src.AssetItems)
                );
        }
    }
}